const validateEmail = (element) => {
    let email = element.value
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
}

const validateName = (element) => {
    let name = element.value
    const re = /^[a-zA-Zа-яА-Я\-]+$/
    return re.test(String(name).toLowerCase())
}

const validatePhone = (element) => {
    let phone = element.value
    const re = /^((\+?7|8)[ \-]?)?((\(\d{3}\))|(\d{3}))?([ \-])?(\d{6}[\- ]?\d{2}[\- ]?\d{2})$/
    return re.test(String(phone).toLowerCase())
}

const validatePostCode = (element) => {
    let zipCode = element.value
    const re = /^\d{6}$/
    return re.test(String(zipCode).toLowerCase())
}

const validatePassword = (element) => {
    let password = element.value
    const re = /(?=[^A-Z]*[A-Z])(?=[^!@#\$%]*[!@#\$%])/
    return re.test(String(password).toLowerCase())
}

const validateUsername = (element) => {
    let password = element.value
    const re = /^[a-zA-Z0-9\-_]+$/
    return re.test(String(password).toLowerCase())
}

export const isValidate = (element) => {
    if (element.value.length > 1) {
        if (element.name === 'username') return validateUsername(element)
        if (element.name === 'password') return validatePassword(element)
        if (element.name === 'post-code') return validatePostCode(element)
        if (element.name === 'phone') return validatePhone(element)
        if (element.name === 'email') return validateEmail(element)
        if (element.name === 'first-name' || element.name === 'last-name') return validateName(element)
        return true
    } else return false
}

