import vars from "../_vars";
import {currencyConversion} from './convert-currency'

// ------- удаление товаров с экрана и HTML разметки (для анимации)
export const removeProducts = function (products, delay, animateClass) {
    Array.from(products).reverse().forEach( (e, i) => {
        setTimeout(() => e.classList.remove(animateClass), i * delay)
    })
    return new Promise((resolve) => {
        setTimeout(() => {
            products.forEach(e => e.remove())
            resolve()
        }, products.length * delay + 300  )
    })
}

// ------- показ товаров на экране уже полученных ранее (для анимации)
export const showProducts = function (products, delay, animateClass) {
    products.forEach( (e, i) => {
        setTimeout(() => e.classList.add(animateClass), i * delay + delay)
    })
}

// ------- получение данных по товарам
export const fetchProducts = function (url) {
    return fetch(url).then((response) => response.json()).then((data) => data)
}

// ------- рендеринг товаров в HTML разметке
export const renderProducts = function (data, blockListElement, blockItemClass, initialProduct, finalProduct) {
    let bestBottom = '',
        saleTop = '',
        newBottom = '',
        hotBottom = '',
        newImagesArray = [],
        currency = localStorage.getItem('currency') || '$'
    if (finalProduct > data.length) finalProduct = data.length
    // цикл только по заданному отрезку выборки данных
    for (let i = initialProduct; i < finalProduct; i++) {
        saleTop = data[i].isSale === "true" ? '<span class="product__sale">Sale</span>' : ''
        hotBottom = data[i].isSale === "true" ? '<span class="product-prop product__prop hot">Hot</span>' : ''
        bestBottom = data[i].isBest === "true" ? '<span class="product-prop product__prop best">Best seller</span>' : ''
        newBottom = data[i].isNew === "true" ? '<span class="product-prop product__prop new">New</span>' : ''
        // если на главной странице, label на картинке товара не отображается
        if (vars.$mainProductsList)  saleTop = ''
        blockListElement.insertAdjacentHTML('beforeend', `
            <li class="${blockItemClass}">
                <article class="product" data-id="${data[i].id}">
                    <div class="product__image">
                        ${saleTop}
                        <img src="${data[i].image}" alt="${data[i].title}">
                    </div>
                    ${newBottom}${bestBottom}${hotBottom}                           
                    <h3 class="product__title">
                        <a href="card.html">${data[i].title}</a>
                    </h3>
                    <div class="product-custom">
                        <div class="product-custom__price">
                            <span class="js-conventional-unit">${currency}</span><span class="product__price" data-usd-value=${data[i].price}>${data[i].price}</span>
                            <span class="js-conventional-unit">${currency}</span><span class="product__old-price" data-usd-value=${data[i].old_price}>${data[i].old_price}</span>
                        </div>
                        <div class="product-custom__cart">
                            <button class="btn-reset to-cart">Add to cart</button>
                        </div>
                    </div>
                </article>
            </li>                        
        `)
        newImagesArray.push(blockListElement.querySelector(`[data-id="${data[i].id}"] img`))
    }
    currencyConversion()
    return newImagesArray
}

// -------  анализ содержимого корзины и отключение кнопки add to cart на этих элементах
export const disableProducts = () => {
    let localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach(a => {
        if (document.querySelector(`.product[data-id="${a.id}"]`)) {
            document.querySelector(`.product[data-id="${a.id}"]`)
                .querySelector('.to-cart').disabled = true
        }
    })
}