// -------  функция слежения за полной загрузкой элемента
export const controlLoadElement = function(element) {
    return new Promise ((resolve, reject) => {
        element.onload = () => resolve(element)
        element.onerror = () => reject()
    })
}