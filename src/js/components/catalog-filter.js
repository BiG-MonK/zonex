import vars from '../_vars'

if (document.querySelector('.catalog')) {

    vars.$catalogFiltersTop.forEach(el => {
        el.addEventListener('click', e => {
            e.currentTarget.closest('.catalog-filter').classList.toggle('catalog-filter--open')
        })
    })

    vars.$hideFilters.addEventListener('click', e => {
        vars.$catalogFiltersTop.forEach(el => {
            el.closest('.catalog-filter').classList.remove('catalog-filter--open')
        })
    })

    // --------- Функция создания элемента с названием выбранного фильтра в строку над товарами
    const createChoiceItem = (text) => {
        vars.$catalogChoice.insertAdjacentHTML('afterbegin', `
              <button class="btn-reset catalog-choice__item" data-choice-text="${text}">
                ${text}
                <svg aria-hidden="true">
                  <use xlink:href="img/sprite.svg#close"></use>
                </svg>
              </button>
            `)
        return vars.$catalogChoice.querySelector(`[data-choice-text="${text}"]`)
    }

    // --------- Функция если выбор снят
    const boxUnChecked = (checkBoxWrap) => {
        let dataText = checkBoxWrap.querySelector('.custom-checkbox').dataset.text
        checkBoxWrap.classList.remove('custom-checkbox--active')
        checkBoxWrap.querySelector('input').checked = false
        vars.$catalogChoice.querySelector(`[data-choice-text="${dataText}"]`)
            .classList.remove('catalog-choice__item--is-visible')
        setTimeout(() => {
            vars.$catalogChoice.querySelector(`[data-choice-text="${dataText}"]`).remove()
        }, 500)
    }

    // --------- Функция если выбор поставлен
    const boxChecked = (checkBoxWrap) => {
        let dataText = checkBoxWrap.querySelector('.custom-checkbox').dataset.text
        checkBoxWrap.classList.add('custom-checkbox--active')
        checkBoxWrap.querySelector('input').checked = true
        vars.$catalogChoice.insertAdjacentHTML('afterbegin', `
            <button class="btn-reset catalog-choice__item" data-choice-text="${dataText}">
              ${dataText}
              <svg aria-hidden="true">
                <use xlink:href="img/sprite.svg#close"></use>
              </svg>
            </button>
          `)
        let newElement = vars.$catalogChoice.querySelector(`[data-choice-text="${dataText}"]`)
        newElement.addEventListener('mouseover', () => newElement.classList.add('catalog-choice__item--hover'))
        newElement.addEventListener('mouseout', () => newElement.classList.remove('catalog-choice__item--hover'))
        setTimeout(() => newElement.classList.add('catalog-choice__item--is-visible'), 100)
    }

    const deleteElement = (element, animateClass) => {
        const delay = 500
            element.classList.remove(animateClass)
        return new Promise((resolve) => {
            setTimeout(() => {
                element.remove()
                resolve()
            }, delay)
        })
    }

    // обработчик самих фильтров
    vars.$catalogFilterItems.forEach(checkBoxWrap => {
        checkBoxWrap.querySelector('input').addEventListener('change', e => {
            let isChecked = checkBoxWrap.querySelector('input').checked,
                filterQuantity =  checkBoxWrap.closest('.catalog-filter').querySelector('.catalog-filter__quantity'),
                firstCheckBoxWrap = e.target.closest('.catalog-filter__items').firstElementChild,
                allCheckBoxWrap = e.target.closest('.catalog-filter__items').children,
                currentCheckBoxWrap = e.target.closest('.catalog-filter__item')

            isChecked ? boxChecked(checkBoxWrap) : boxUnChecked(checkBoxWrap)
            let activeAll = vars.$catalogFilters.querySelectorAll('.custom-checkbox--active'),
                activeInternal = checkBoxWrap.closest('.catalog-filter__items').querySelectorAll('.custom-checkbox--active')

            if (currentCheckBoxWrap === firstCheckBoxWrap) {
                if (e.target.checked) {
                    activeInternal.forEach(checkBoxWrap => {
                        if (checkBoxWrap !== firstCheckBoxWrap) boxUnChecked(checkBoxWrap)
                    })
                    allCheckBoxWrap.forEach(checkBoxWrap => {
                        if (checkBoxWrap !== firstCheckBoxWrap) checkBoxWrap.querySelector('input').disabled = true
                    })
                    filterQuantity.textContent = '1'
                } else {
                    allCheckBoxWrap.forEach(checkBoxWrap => checkBoxWrap.querySelector('input').disabled = false)
                    filterQuantity.textContent = '0'
                }
            } else filterQuantity.textContent = String(activeInternal.length)
            activeAll.length > 0 ? vars.$catalogChoice.classList.add('catalog-choice--is-visible')
                                 : vars.$catalogChoice.classList.remove('catalog-choice--is-visible')
        })
    })

    // обработчик событий строки отображения какие фильтра выбраны (над списком товаров)
    vars.$catalogChoice.addEventListener('click', (e) => {
        let text = e.target.textContent.trimLeft().trimRight()
        if (e.target.classList.contains('catalog-choice__clear')) {
            Array.from(e.currentTarget.children).forEach(el => {
                if (!el.classList.contains('catalog-choice__clear')) {
                    deleteElement(el, 'catalog-choice__item--is-visible')
                }
                document.querySelectorAll('.catalog-filter__quantity').forEach(el => el.textContent = '0')
                vars.$catalogFilterItems.forEach(elem => {
                    elem.querySelector('input').checked = false
                    elem.querySelector('input').disabled = false
                    elem.classList.remove('custom-checkbox--active')
                })
            })
            vars.$catalogChoice.classList.remove('catalog-choice--is-visible')
        } else {
            let currentCheckBox = document.querySelector(`[data-text="${text}"]`)
            let currentCheckBoxWrap = currentCheckBox.closest('.catalog-filter__item')
            let firstCheckBoxWrap = currentCheckBox.closest('.catalog-filter__items').firstElementChild
            if (e.target.classList.contains('catalog-choice__item')) {
                e.target.classList.remove('catalog-choice__item--hover')
                deleteElement(e.target, 'catalog-choice__item--is-visible')
                    .then(() => {
                        if (vars.$catalogChoice.children.length === 1) {
                            vars.$catalogChoice.classList.remove('catalog-choice--is-visible')
                        }
                    })
                currentCheckBox.querySelector('input').checked = false
                currentCheckBox.closest('.catalog-filter__item').classList.remove('custom-checkbox--active')
                currentCheckBox.closest('.catalog-filter').querySelector('.catalog-filter__quantity').textContent--
            }
            if (currentCheckBoxWrap === firstCheckBoxWrap) {
                currentCheckBox.closest('.catalog-filter__items').querySelectorAll('.catalog-filter__item')
                    .forEach((e) => e.querySelector('input').disabled = false)
            }
        }
    })
}