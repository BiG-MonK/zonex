import vars from '../_vars'
import {throttler as resizeThrottler} from '../functions/trottler'

let currentWidth = window.innerWidth

vars.$cardDescriptionLink.forEach(el => {
    //  Переключение вкладок на странице товара
    el.addEventListener('click', e => {
        e.preventDefault()
        let heightDescriptionLeft = 0
        let target = e.currentTarget.getAttribute('href'),
            activeContent = vars.$cardDescription.querySelector('.card-description__content--active'),
            newActiveContent = vars.$cardDescription.querySelector(`[data-target="${target}"]`)
        if (window.innerWidth < 576) {
            heightDescriptionLeft = parseInt(getComputedStyle(vars.$cardDescription.querySelector('.card-description__left')).height)
        }
        vars.$cardDescription.style.height = heightDescriptionLeft + parseInt(getComputedStyle(activeContent).height) + 51 + 'px'
        vars.$cardDescriptionLink.forEach(el => el.classList.remove('card-description__link--active'))
        e.currentTarget.classList.add('card-description__link--active')
        activeContent.classList.remove('card-description__content--active')
        setTimeout(() => {
            activeContent.classList.add('is-delete')
            newActiveContent.classList.remove('is-delete')
            vars.$cardDescription.style.height = heightDescriptionLeft +
                parseInt(getComputedStyle(vars.$cardDescription.querySelector('.card-description__right')).height) + 'px'
            setTimeout(() => newActiveContent.classList.add('card-description__content--active'), 20)
        }, 300)
    })
})


// -------  для перерасчета высоты блока description при изменении размера экрана
const calcSizeDescription = function () {
    if (vars.$cardDescription) {
        if (currentWidth !== window.innerWidth) {
            currentWidth = window.innerWidth
            let heightDescriptionLeft = 0
            let activeContent = vars.$cardDescription.querySelector('.card-description__content--active')
            if (window.innerWidth < 576) {
                heightDescriptionLeft = parseInt(getComputedStyle(vars.$cardDescription.querySelector('.card-description__left')).height)
            }
            vars.$cardDescription.style.height = heightDescriptionLeft + parseInt(getComputedStyle(activeContent).height) + 51 + 'px'
        }
    }
}

// -------  обработчик события на изменение размера экрана, с использованием троттлинга
window.addEventListener("resize", () => resizeThrottler(calcSizeDescription, 1000), false)