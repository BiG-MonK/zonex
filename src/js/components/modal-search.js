import vars from '../_vars';
import {modalWindowOn, modalWindowOff} from '../functions/modalWindows'
import {controlLoadElement} from '../functions/controlLoadElement'
import {fetchProducts, removeProducts, renderProducts, showProducts} from '../functions/productActions'

if (vars.$searchModal) {

    const searchField = vars.$searchModal.querySelector('.form-search__field')
    const resultsContainer = vars.$searchModal.querySelector('.form-search__results-container')
    const resultsBlock = vars.$searchModal.querySelector('.form-search__results-inner')
    const searchEmpty = vars.$searchModal.querySelector('.form-search__empty')
    const delayInput = 1500             // задержка отрабатывания ввода в поле поиска
    const delayRemoveProducts = 60      // задержка очищения блока поиска от старых продуктов
    const delayShowProducts = 70        // задержка появления новых продуктов в блоке поиска
    let inputFlag   //  технический флаг для троттлинга ввода
    // обработчик открытия модального окна поиска
    vars.$search.addEventListener('click', e => {
        modalWindowOn(vars.$searchModal, 'search-modal--visible')
        searchField.focus()
    })

    // обработчик закрытия модального окона поиска по клику
    vars.$searchModal.addEventListener('click', e => {
        if (e.target.closest('.search-modal__close')) {
            modalWindowOff(vars.$searchModal, 'search-modal--visible')
            resultsContainer.classList.remove('form-search__results-container--visible')
        }
    })

    //  событие ввода в поисковое поле
    vars.$searchModal.querySelector('.form-search__field').addEventListener('input', e => {
        if (!inputFlag) {
            inputFlag = setTimeout(function () {
                inputFlag = null
                searchProducts()
            }, delayInput)
        }
    })

    const searchProducts = function () {
        if (searchField.value.length >= 0) {
            resultsContainer.classList.add('form-search__results-container--visible')
            removeProducts(resultsBlock.querySelectorAll('.form-search__product'), delayRemoveProducts , 'emergence')
                .then(() => fetchProducts('jsonResponses/products.json')
                    .then((data) => {
                        let dataStore = [...data]
                        let out = []
                        if (searchField.value.length !== 0) {
                            out = dataStore.filter(e => e.title.toLowerCase().includes(searchField.value))
                        } else {
                            resultsBlock.style.height = '0'
                        }
                        return renderProducts(out, resultsBlock, 'form-search__product', 0, out.length)
                    })
                    .then((goods) => Promise.all(goods.map(controlLoadElement)))
                    .then((images) => {
                        if (images.length > 0) {
                            searchEmpty.classList.add('form-search__empty--hidden')
                            let foundProducts = images.map(el => el.closest('.form-search__product')),
                                productItem = foundProducts[0].closest('.form-search__product'),
                                heightProductItem = parseInt(getComputedStyle(productItem).height) +
                                    parseInt(getComputedStyle(productItem).marginBottom) + 16, // если в названии продукта две строчки
                                widthProductItem = parseInt(getComputedStyle(productItem).width) +
                                    parseInt(getComputedStyle(productItem).marginLeft) +
                                    parseInt(getComputedStyle(productItem).marginRight),
                                widthBlockResult = parseInt(getComputedStyle(resultsBlock).width),
                                quantityPerLine = Math.floor(widthBlockResult / widthProductItem),
                                quantityPerColumn = Math.ceil(foundProducts.length / quantityPerLine),
                                maxHeightBlockResult = parseInt(getComputedStyle(resultsBlock).maxHeight),
                                calcHightBlockResult = quantityPerColumn * heightProductItem +
                                    parseInt(getComputedStyle(resultsBlock).marginTop) +
                                    parseInt(getComputedStyle(resultsBlock).marginBottom) +
                                    parseInt(getComputedStyle(resultsBlock).paddingTop) +
                                    parseInt(getComputedStyle(resultsBlock).paddingBottom)
                            resultsBlock.style.height = calcHightBlockResult > maxHeightBlockResult ? maxHeightBlockResult + 'px'
                                : calcHightBlockResult + 'px'
                            return showProducts(foundProducts, delayShowProducts, 'emergence')
                        } else {
                            searchEmpty.classList.remove('form-search__empty--hidden')
                            resultsBlock.style.height = '60px'
                        }
                    })
                    .catch(err => console.error(err)))
        } else {
            resultsContainer.classList.remove('form-search__results-container--visible')
        }
    }
}