import vars from "../_vars";

let currentUrl = window.location.href

// определение какую html страницу загружать
if (currentUrl.includes('index.html') || !currentUrl.includes('html')) {
    vars.$header.className = 'header'
    vars.$headerContainer.className = 'container header__container header-home-narrow'
    document.querySelector('[data-url="index"]').classList.add('nav__link--current')
} else {
    vars.$header.className = 'header catalog-header'
    vars.$headerContainer.className = 'container header__container container-narrow'
    document.querySelector('[data-url="catalog"]').classList.add('nav__link--current')
}
// --- закрытие полоски рекламы вверху
if (vars.$freeDelivery) {
    vars.$freeDeliveryBtn.addEventListener('click', e => {
        e.currentTarget.closest('.free-delivery').classList.add('free-delivery--hidden')
        vars.$header.style.paddingTop = '0'
        setTimeout(() => vars.$freeDeliveryBtn.remove(), 300)
    })
}

// --- прилипание к верху header по скроллу
window.addEventListener('scroll', () => {
    if (pageYOffset > 50) {
        vars.$header.classList.add('header--sticky')
    } else {
        vars.$header.classList.remove('header--sticky')
    }
})