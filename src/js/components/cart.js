import vars from '../_vars'
import {productionPrice, priceWithoutSpaces, printTotalPrice} from '../functions/priceActions'
import {getTotalPrice, getTotalQuantity} from '../functions/calculate'
import {controlLoadElement} from '../functions/controlLoadElement'
import {stepper} from '../functions/stepper'
import {removeProducts, showProducts} from '../functions/productActions'
import {getCurrencyFactor} from '../functions/convert-currency'
import {isValidate} from '../functions/validation'


if (document.querySelector('.cart-page')) {
    const messageUndo = vars.$cartContent.querySelector('.messages__undo'),
          messageEmpty = vars.$cartContent.querySelector('.messages__empty'),
          messageUndoText = messageUndo.querySelector('.messages__undo-text'),
          messageUndoBtn = messageUndo.querySelector('.messages__undo-btn'),
          messageError = vars.$cartContent.querySelector('.messages__error'),
          shoppingForm = vars.$cartContent.querySelector('.shopping__form'),
          shoppingCheck = vars.$cartContent.querySelector('.shopping__check'),
          cartProductsList = vars.$cartContent.querySelector('.shopping-form__cart-list'),
          checkProductList = vars.$cartContent.querySelector('.check__product-list'),
          subtotalSum = vars.$cartContent.querySelectorAll('.check__subtotal-sum'),
          shippingSum = vars.$cartContent.querySelector('.check__shipping-sum'),
          totalSum = vars.$cartContent.querySelectorAll('.check__total-sum'),
          totalQuantityProducts = vars.$cartNavigationList.querySelector('.cart-navigation__link-quantity'),
          cartNavigationLinks = vars.$cartNavigationList.querySelectorAll('.cart-navigation__link'),
          placeOrderBtn = vars.$cartContent.querySelector('.check__place-order-btn'),
          additionalShippingFields = vars.$cartContent.querySelectorAll('.checkout-form__shipping-block input'),
          additionalShippingBlock = vars.$cartContent.querySelector('.checkout-form__shipping-block')
    let shippingSumValue = parseFloat(shippingSum.textContent) || 1 ? 0 : parseFloat(shippingSum.textContent).toFixed(2),
        localStore = JSON.parse(localStorage.getItem('cart')) || [],
        deletedProduct = [],
        heightCartItem = 0,
        heightProductsList = 0,
        isDifferentAddress = false

    //  html разметка товара в главной корзине
    const htmlCartItem = function(currency, id, img, title, priceString, quantity, priceAll, priceUsdValue, priceAllUsdValue) {
        return `
        <article class="shopping-form__cart-item cart-item" data-id="${id}">
            <div class="cart-item__thumbnail">
                <button class="cart-item__remove--mobile btn-reset" type="button">
                    <svg class="cart-item__remove-icon">
                        <use xlink:href="img/sprite.svg#close" aria-label="remove a product from your cart"></use>
                    </svg>
                </button>
                <a class="cart-item__image-wrapper" href="#">
                    <img src="${img}" alt="product's picture" loading="lazy">
                </a>
            </div>
            <div class="cart-item__name">
                <a href="#" class="cart-item__name-ref">${title}</a>
            </div>
            <div class="cart-item__price">
                <span class="js-conventional-unit">${currency}</span>
                <span class="cart-item__amount" data-usd-value=${priceUsdValue}>${priceString}</span>
            </div>
            <div class="cart-item__quantity">
                <div class="cart-item__stepper stepper">
                    <button type="button" class="btn-reset stepper__btn stepper__btn--minus" aria-label="minus">
                        <svg class="">
                            <use xlink:href="img/sprite.svg#minus"></use>
                        </svg>
                    </button>
                    <input type="text" class="stepper__input" value="${quantity}" maxlength="5">
                    <button type="button" class="btn-reset stepper__btn stepper__btn--plus" aria-label="plus">
                        <svg class="">
                            <use xlink:href="img/sprite.svg#plus"></use>
                        </svg>
                    </button>
                </div> <!-- /.stepper -->
            </div>
            <div class="cart-item__subtotal">
                <span class="js-conventional-unit">${currency}</span>
                <span class="cart-item__items-sum" data-usd-value=${priceAllUsdValue}>${priceAll}</span>
            </div>
            <div class="cart-item__remove">
                <button class="cart-item__remove-btn btn-reset" type="button">
                    <svg class="cart-item__remove-icon">
                        <use xlink:href="img/sprite.svg#close" aria-label="remove a product from your cart"></use>
                    </svg>
                </button>
            </div>
		</article> <!-- /.cart-item -->
        `
    }

    //  html разметка товара в чеклисте
    const htmlCheckListItem = function(currency, title, quantity, priceAll, priceAllUsdValue) {
        return `
            <li class="check__list-item">
                <div class="check__product-name">${title}</div>
                <div class="check__product-quantity">x ${quantity}</div>
                <div class="check__product-price">
                    <span class="js-conventional-unit">${currency}</span>
                    <span class="check__price-sum" data-usd-value=${priceAllUsdValue}>${priceAll}</span>
                </div>
            </li>
        `
    }

    //  html разметка сообщения об ошибке валидации полей
    const htmlMessageErrorItem = function(textField) {
        return `
                <li class="messages__error-item">
                    <span class="messages__error-text">${textField}</span>
                </li>
            `
    }

    //  удаление из главной корзины выбранного товара
    const deleteCartItem = (product) => {
        let id = product.dataset.id
        localStore = JSON.parse(localStorage.getItem('cart')) || []
        localStore.forEach((a, i) => {
            if (a.id === id) deletedProduct = localStore.splice(i, 1)
        })
        localStorage.setItem('cart', JSON.stringify(localStore))
        product.classList.add('is-reduce')
        removeProducts([product], 100 , 'emergence')
            .then(() => {
                totalQuantityProducts.textContent = getTotalQuantity()
                subtotalSum.forEach(el => printTotalPrice(el))
                totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
                cartProductsList.style.height = heightCartItem * localStore.length + 'px'
                controlEmptyCart(localStore.length)
                messageUndo.classList.remove('is-reduce')
                messageUndoText.textContent = deletedProduct[0].title
            }).catch(err => console.error(err))
        renderCheckListItems(localStore)
    }

    //  контроль наличия товара в главной корзине
    const controlEmptyCart = (quantity) => {
        if (quantity === 0) {
            shoppingForm.classList.add('is-reduce')
            shoppingCheck.classList.add('is-reduce')
            setTimeout(() => messageEmpty.classList.remove('is-reduce'), 10)
            totalQuantityProducts.textContent = '0'
            vars.$cartNavigationList.querySelector('[href="#checkout"]').classList.add('is-disabled')
        } else {
            messageEmpty.classList.add('is-reduce')
            setTimeout(() => {
                shoppingForm.classList.remove('is-reduce')
                shoppingCheck.classList.remove('is-reduce')
            }, 510)
            vars.$cartNavigationList.querySelector('[href="#checkout"]').classList.remove('is-disabled')
        }
    }

    //  функция для обработчика событий на stepper продуктах внутри главной корзины
    const stepperEvent = e => {
        const product = e.target.closest('.cart-item')
        const itemsSum = product.querySelector('.cart-item__items-sum')
        let inputValue = parseInt(product.querySelector('.stepper__input').value),
            price = parseFloat(priceWithoutSpaces(product.querySelector('.cart-item__amount').textContent)),

        localStore = JSON.parse(localStorage.getItem('cart')) || []
        let idProduct = product.dataset.id
        localStore.forEach(a => {
            if (a.id === idProduct) {
                a.quantity = inputValue
                itemsSum.dataset.usdValue = (inputValue * a.priceUsdValue).toFixed(2)
            }
        })
        localStorage.setItem('cart', JSON.stringify(localStore))

        totalQuantityProducts.textContent = getTotalQuantity()
        subtotalSum.forEach(el => printTotalPrice(el))
        totalSum.forEach(el => el.textContent = productionPrice(getTotalPrice().toFixed(2)))
        itemsSum.textContent = productionPrice((inputValue * price).toFixed(2)).toString()
        renderCheckListItems(localStore)
    }

    //  рендер товаров для главной корзины из localstorage
    const renderCartItems = (products) => {
        let newImagesArray = []
        let currency = localStorage.getItem('currency') || '$'
        let currencyFactor = getCurrencyFactor()
        return new Promise(resolve => {
            products.forEach(a => {
                let price = productionPrice((priceWithoutSpaces(a.priceUsdValue) * currencyFactor).toFixed(2)),
                    priceAll = productionPrice((priceWithoutSpaces(a.priceUsdValue) * currencyFactor * a.quantity).toFixed(2)),
                    priceAllUsdValue = (priceWithoutSpaces(a.priceUsdValue) * a.quantity).toFixed(2)
                cartProductsList.insertAdjacentHTML('beforeend',
                    htmlCartItem(currency, a.id, a.img, a.title, price, a.quantity, priceAll, a.priceUsdValue, priceAllUsdValue))
                newImagesArray.push(cartProductsList.querySelector(`[data-id="${a.id}"] img`))
            })
            resolve(newImagesArray)
        })
    }

    //  рендер товаров для чеклиста из localstorage
    const renderCheckListItems = (products) => {
        let currency = localStorage.getItem('currency') || '$'
        let currencyFactor = getCurrencyFactor()
        checkProductList.innerHTML = ''
        products.forEach(a => {
            let priceAll = productionPrice((priceWithoutSpaces(a.priceUsdValue) * currencyFactor * a.quantity).toFixed(2)),
                priceAllUsdValue = (priceWithoutSpaces(a.priceUsdValue) * a.quantity).toFixed(2)
            checkProductList.insertAdjacentHTML('beforeend',
                htmlCheckListItem(currency, a.title, a.quantity, priceAll, priceAllUsdValue))
        })
    }

    //  генерирование продуктов и расчет сумм внутри главной корзины
    const renderCartContent = (products) => {
        let refCheckout = JSON.parse(localStorage.getItem('refCheckout')) || ''
        if (refCheckout === true) {  // переход сразу на блок checkout без анимаций
            vars.$cartContent.querySelector('.shopping').classList.add('is-delete')
            vars.$cartContent.querySelector('.checkout').classList.remove('is-delete')
            vars.$cartNavigationList.querySelector('[href="#shopping"]').classList.remove('cart-navigation__link--active')
            vars.$cartContent.querySelector('.shopping').classList.remove('cart-content__tab--active')
            vars.$cartNavigationList.querySelector('[href="#checkout"]').classList.add('cart-navigation__link--active')
            vars.$cartContent.querySelector('.checkout ').classList.add('cart-content__tab--active')
            localStorage.removeItem('refCheckout')
        }
        renderCartItems(products)
            .then((images) => Promise.all(images.map(controlLoadElement)))
            .then((images) => {
                let cartItems = []
                heightCartItem = parseInt(getComputedStyle(cartProductsList.querySelector('.cart-item')).height)
                heightProductsList = parseInt(getComputedStyle(cartProductsList).height)
                cartProductsList.style.height = heightCartItem * localStore.length + 'px'
                images.forEach(el => {
                    const product = el.closest('.cart-item'),
                          stepperInput = product.querySelector('.stepper__input'),
                          stepperMinus = product.querySelector('.stepper__btn--minus'),
                          stepperPlus = product.querySelector('.stepper__btn--plus')
                    stepper(stepperInput, stepperMinus, stepperPlus)
                    stepperPlus.addEventListener('click', stepperEvent)
                    stepperMinus.addEventListener('click', stepperEvent)
                    stepperInput.addEventListener('blur', stepperEvent)
                    product.style.height = heightCartItem + 'px'
                    cartItems.push(product)
                })
                totalQuantityProducts.textContent = getTotalQuantity()
                showProducts(cartItems, 100, 'emergence')
            }).catch(err => console.error(err))
        renderCheckListItems(localStore)
        subtotalSum.forEach(el => printTotalPrice(el))
        totalSum.forEach(el => el.textContent = productionPrice((getTotalPrice() + shippingSumValue).toFixed(2)))
    }

    // ------- наведение курсора на блок товаров в главной корзине
    cartProductsList.addEventListener('mouseover', e => {
        if (e.target.closest('.cart-item__remove-btn')) {
            let current = e.target.closest('.cart-item')
            current.querySelector('.cart-item__thumbnail').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__name').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__price').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__quantity').classList.add('cart-item--remove-hover')
            current.querySelector('.cart-item__subtotal').classList.add('cart-item--remove-hover')
        }
    })

    // ------- покидание курсора с блока товаров в главной корзине
    cartProductsList.addEventListener('mouseout', e => {
        if (e.target.closest('.cart-item__remove-btn')) {
            let current = e.target.closest('.cart-item')
            current.querySelector('.cart-item__thumbnail').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__name').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__price').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__quantity').classList.remove('cart-item--remove-hover')
            current.querySelector('.cart-item__subtotal').classList.remove('cart-item--remove-hover')
        }
    })

    // ------- клик по кнопке удалить в главной корзине
    cartProductsList.addEventListener('click', e => {
        if (e.target.closest('.cart-item__remove-btn') || e.target.closest('.cart-item__remove--mobile')) {
            deleteCartItem(e.target.closest('.cart-item'))
        }
    })

    // ------- нажатие кнопки вернуть удаленный товар из главной корзины
    messageUndoBtn.addEventListener('click', () => {
        localStore = JSON.parse(localStorage.getItem('cart')) || []
        localStore.push(deletedProduct[0])
        localStorage.setItem('cart', JSON.stringify(localStore))
        messageUndo.classList.add('is-reduce')
        renderCartContent(deletedProduct)
        controlEmptyCart(localStore.length)
        deletedProduct = []
    })

    // ------- нажатие навигационного меню на странице главной корзины
    cartNavigationLinks.forEach(el => {
        el.addEventListener('click', e => {
            const activeLink = vars.$cartNavigationList.querySelector('.cart-navigation__link--active'),
                  newActiveLink = e.target,
                  activeContent = vars.$cartContent.querySelector('.cart-content__tab--active'),
                  newActiveContent = vars.$cartContent
                      .querySelector(`[data-section="${newActiveLink.getAttribute('href')}"]`)
            e.preventDefault()
            if (activeLink !== newActiveLink) {
                activeLink.classList.remove('cart-navigation__link--active')
                newActiveLink.classList.add('cart-navigation__link--active')
                activeContent.classList.remove('cart-content__tab--active')
                setTimeout(() => {
                    activeContent.classList.add('is-delete')
                    newActiveContent.classList.remove('is-delete')
                    setTimeout(() => newActiveContent.classList.add('cart-content__tab--active'), 10)
                }, 500)
            }
        })
    })

    // ------- нажатие кнопки продолжить оформление заказа
    vars.$cartContent.querySelector('.check__to-checkout-btn').addEventListener('click', e => {
        e.preventDefault()
        vars.$cartNavigationList.querySelector('[href="#checkout"]').click()
    })

    // ------- выбор способа оплаты в блоке transfer
    vars.$cartContent.querySelectorAll('.transfer__item input').forEach(el => {
        el.addEventListener('change', e => {
            let method = vars.$cartContent.querySelector('input[name="transfer"]:checked').value
            if (method === 'payments') {
                placeOrderBtn.textContent = 'Proceed to payment'
            } else {
                placeOrderBtn.textContent = 'Place order'
            }
        })
    })

    // ------- нажатие на опцию иного адреса доставки
    vars.$cartContent.querySelector('.checkout-form__different input').addEventListener('change', () => {
        additionalShippingBlock.classList.toggle('checkout-form__shipping-block--open')
        if (additionalShippingBlock.classList.contains('checkout-form__shipping-block--open')) {
            isDifferentAddress = true
            additionalShippingFields.forEach( el => {
                el.removeAttribute('disabled')
            })
            setTimeout(() => additionalShippingFields[0].focus(), 500)
        } else {
            isDifferentAddress = false
            additionalShippingFields.forEach( el => {
                el.setAttribute('disabled', 'true')
            })
        }
    })

    // ------- нажатие на сообщение "have a coupon"
    vars.$cartContent.querySelector('.messages__coupon-show').addEventListener('click', () => {
        vars.$cartContent.querySelector('.form-coupon').classList.toggle('form-coupon--visible')
        vars.$cartContent.querySelector('.form-coupon .coupon__text').focus()
    })

    // ------- нажатие на сообщение "returning customer"
    vars.$cartContent.querySelector('.messages__customer-login').addEventListener('click', () => {
        vars.$cartContent.querySelector('.checkout-login').classList.toggle('checkout-login--visible')
        vars.$cartContent.querySelector('.checkout-login .checkout-login__username').focus()
    })

    // ------- обработка ввода полей формы
    vars.$cartContent.querySelector('.checkout__form').addEventListener('input', e => {
        let element = e.target
        if (element.classList.contains('form-field')) {
            if (isValidate(element)) {
                element.classList.remove('error-field')
            } else {
                // element.classList.add('error-field')
            }
        }
    })

    // ------- submit формы отправки купона во вкладке shopping cart
    vars.$cartContent.querySelector('.shopping__form').addEventListener('submit', e => {
        const couponMessageEl = vars.$cartContent.querySelector('.shopping__form .coupon__message')
        let enteredText = vars.$cartContent.querySelector('.shopping__form .coupon__text').value
        let messageText = enteredText ? `Coupon "${enteredText}" does not exist!` : 'Enter coupon code'
        e.preventDefault()
        couponMessageEl.style.height = '0px'
        couponMessageEl.innerHTML = ''
        setTimeout(() => {
            couponMessageEl.style.height = '16px'
            couponMessageEl.innerHTML = `<div class="coupon__message">${messageText}</div>`
            couponMessageEl.classList.remove('is-reduce')
        }, 500)
    })

    // ------- submit формы авторизации во вкладке checkout "returning customer"
    vars.$cartContent.querySelector('.checkout__form-login').addEventListener('submit', e => {
        let messageText = 'Unknown username. Check again or try your email address.'
        e.preventDefault()
        vars.$messageErrorList.innerHTML = ''
        messageError.style.height = '40px'
        messageError.classList.remove('is-reduce')
        setTimeout(() => {
            vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(messageText))
        }, 300)
    })

    // ------- submit формы отправки купона во вкладке checkout "have a coupon"
    vars.$cartContent.querySelector('.checkout__form-coupon').addEventListener('submit', e => {
        let enteredText = vars.$cartContent.querySelector('.checkout__form-coupon .coupon__text').value
        e.preventDefault()
        vars.$messageErrorList.innerHTML = ''
        messageError.classList.remove('is-reduce')
        messageError.style.height = '40px'
        setTimeout(() => {
            let messageText = enteredText ? `Coupon "${enteredText}" does not exist!` : 'Enter coupon code'
            vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(messageText))
        }, 300)
    })

    // ------- submit основная форма оформления товаров
    vars.$formCheckout.addEventListener('submit', e => {
        e.preventDefault()
        let formData = new FormData( vars.$formCheckout)
        const billingFields = [...vars.$cartContent.querySelectorAll('.checkout-form > input')]
        const shippingFields = [...vars.$cartContent.querySelectorAll('.checkout-form__shipping-block input')]
        const isCheckPolicy = vars.$cartContent.querySelector('.check__policy input').checked
        let quantityErrors = 0,
            fieldsToCheck,
            errorItems = []
        vars.$messageErrorList.innerHTML = ''
        messageError.style.height = '20px'

        if (isCheckPolicy) {
            fieldsToCheck = isDifferentAddress ? billingFields.concat(shippingFields) : billingFields
            fieldsToCheck.forEach(field => {
                if (field.closest('.js-required')) {
                    if (isValidate(field)) {
                        field.classList.remove('error-field')
                    } else {
                        quantityErrors++
                        errorItems.push(field)
                    }
                }
            })
            if (quantityErrors > 0) {
                messageError.classList.remove('is-reduce')
                errorItems.forEach( (e, i) => {
                    let textField = e.previousElementSibling.textContent
                    if (e.closest('.checkout-form__shipping-block')) {
                        textField = 'Shipping ' + textField
                    } else {
                        textField = 'Billing ' + textField
                    }
                    e.classList.add('error-field')
                    messageError.style.height = (parseInt(messageError.style.height) + 20) + 'px'
                    setTimeout(() => {
                        let messageText = `<b>${textField}</b> is a required field.`
                        vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(messageText))
                    }, i * 100 + 50)
                })
            } else {
                messageError.style.height = '0px'
                messageError.classList.add('is-reduce')
            }
        } else {
            messageError.style.height = '40px'
            messageError.classList.remove('is-reduce')
            setTimeout(() => {
                let messageText = 'Please read and accept the terms and conditions to proceed with your order.'
                vars.$messageErrorList.insertAdjacentHTML('beforeend' , htmlMessageErrorItem(messageText))
            }, 300)
        }
        document.querySelector('.checkout__messages').scrollIntoView({
            behavior: "smooth",
            block: "start"
        })
    })

    vars.$cart.style.display = 'none'
    controlEmptyCart(localStore.length)
    if (localStore.length > 0) renderCartContent(localStore)
}